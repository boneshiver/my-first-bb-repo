const express = require('express')
const app = express()

app.use(express.static('public'))

app.get('/ping', (req, res)=>{
    res.send('pong')
})
app.get('/p*', (req, res)=>{
    res.send('p*')
})
//github student pack 
app.listen(8080)